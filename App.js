import 'react-native-gesture-handler';
import {_View} from 'react-native';
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SplashScreen from 'react-native-splash-screen';
import SendScreen from './screens/send/SendScreen';
import ReceiveScreen from './screens/receive/ReceiveScreen';
import AppStack from './stacks/AppStack';
import IntroStack from './stacks/IntroStack';
import {Buffer} from 'buffer';
import './shim.js';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Stack = createNativeStackNavigator();

const APPSTACK = 'AppStack',
  INTROSTACK = 'IntroStack';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      initialRouteState: APPSTACK,
    };
  }

  async componentDidMount() {
    this.setState({isLoaded: true});

    var wif = require('wif');
    var privKey = Buffer.from('0000000000000000000000000000000000000000000000000000000000000001', 'hex')
    var key = wif.encode(128, privKey, true)
  
    console.log('Key = ' + key)
  
    var password = 'superdupersecretpassword'
  
    var bcrypt = require('bcryptjs');
    var pandacoin = require('bitcoinjs-lib')
  
    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(password, salt)
  
    var bool = bcrypt.compareSync(password, hash)
  
    console.log('Hash = ' + hash)
    console.log('Bool = ' + bool)

    SplashScreen.hide();

    (await this.checkIfFirstLaunch()) == INTROSTACK
      ? this.setState({initialRouteState: INTROSTACK})
      : this.setState({initialRouteState: APPSTACK});
  }

  render() {
    console.log('rendering')
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName={INTROSTACK}>
          <Stack.Screen
            name={APPSTACK}
            component={AppStack}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name={INTROSTACK}
            component={IntroStack}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }

  async checkIfFirstLaunch() {
    return (await AsyncStorage.getItem('initialRouteState')) == null
      ? INTROSTACK
      : APPSTACK;
      
  }

  createLaunchSharedPref() {
    SharedPrefs.setName('launch');
    SharedPrefs.setItem('initialRouteState', 0);
    SharedPrefs.setItem('IsWalletOpen', 0);
  }
}

export default App;
