import "react-native-gesture-handler";

const Init = (props) => {
    const { navigation, dispatch } = props
  
    React.useEffect(() => {
      dispatch(appInitService.initApp(navigation))
    }, [dispatch, navigation])
  
    return null
  }
  
  export default connect()(Init)