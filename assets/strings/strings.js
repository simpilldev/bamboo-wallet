import LocalizedStrings from "react-native-localization";
import english from './en'

export const strings = new LocalizedStrings({
  en: {
    welcome: "Welcome to Bamboo",
    briefBambooDescription: "A simple wallet to manage your Pandacoin. Anytime, anywhere.",
    createWallet: "Create Wallet",
    importWallet: "Import Wallet",
    walletImport: "Wallet Import",
    walletImportInstructions: "Please enter your seed words, private or public key, WIF, or anything else you have." 
    + "Bamboo will try to parse the info and import your wallet.\n\nPlease note, using a public key will create a watch-only wallet.",
  }
});