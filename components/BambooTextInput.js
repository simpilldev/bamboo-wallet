import {StyleSheet, TouchableOpacity, _View, TextInput} from 'react-native';
import {SafeAreaView, useSafeAreaInsets} from 'react-native-safe-area-context';
import * as React from 'react';

const BambooTextInput = ({
  backgroundColor,
  placeholderText,
  secureTextEntry,
  fontSize,
  onChangeText,
  onSubmit,
}) => {
  return (
    <TextInput
      style={[
        {backgroundColor: backgroundColor, fontSize: fontSize},
        styles.modalTextInputBox,
      ]}
      placeholder={placeholderText}
      placeholderTextColor="white"
      keyboardType="default"
      blurOnSubmit={true}
      secureTextEntry={secureTextEntry}
      underlineColorAndroid="#f000"
      returnKeyType="done"
      onChangeText={onChangeText}
      onSubmitEditing={onSubmit}
    />
  );
};

const styles = StyleSheet.create({
  modalTextInputBox: {
    width: '85%',
    justifyContent: 'space-around',
    borderRadius: 20,
    borderColor: '#141414',
    paddingLeft: 16,
    borderWidth: 1,
    alignItems: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default BambooTextInput;
