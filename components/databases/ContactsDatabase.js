import SQLite from 'react-native-sqlite-storage';
import {Buffer} from 'buffer';
import '../../shim.js';
global.Buffer = global.Buffer || require('buffer').Buffer;

export default class ContactsDatabaseHelper {
  getDatabaseFile() {
    return SQLite.openDatabase(
      {
        name: 'labels_database',
        location: 'default',
      },
      () => {},
      error => {
        console.log('Error opening database :(');
      },
    );
  }

  createNewTable() {
    this.getDatabaseFile().transaction(tx => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS ' +
          'Labels ' +
          '(ID INTEGER PRIMARY KEY AUTOINCREMENT, Label TEXT, Address TEXT);',
      );
    });
  }

  // getRowCount() {
  //   let count = 0;

  //   this.getDatabaseFile().transaction(tx => {
  //     tx.executeSql('SELECT Label, Address FROM Labels', [], (tx, results) => {
  //       count = results.rows.length;
  //       console.log('Results.rows.length = ' + results.rows.length);
  //       console.log('Count = ' + count);
  //     });
  //   });

  //   return count;
  // }

   async getRowCount() {
      await this.getDatabaseFile().transaction(tx => {
        tx.executeSql('SELECT Label, Address FROM Labels', [], (tx, results) => {
          resolve(results.rows.length)
        });
      });
  }
  

  getContacts() {
    try {
      this.getDatabaseFile().transaction(tx => {
        tx.executeSql(
          'SELECT Label, Address FROM Labels',
          [],
          (tx, results) => {
            if (results.rows.length > 0) {
              let data = [];
              for (let i = 0; i < results.rows.length; i++) {
                let item = results.rows.item(i);
                console.log(item);
                data.push({label: item.Label, address: item.Address});
                console.log(item.Label);
              }
            } else {
              console.log('The database is empty :(');
            }
          },
        );
      });
    } catch {
      console.log('Error fetching data.');
    }
  }

  insertNewContact(label, address) {
    console.log('Label = ' + label);
    console.log('Address = ' + address);

    if (label.length == 0 || address.length == 0) {
      console.log('No data.');
      return false;
    } else if (!this.checkAddressValidity(address)) {
      console.log("Uh Oh, that doesn't look like a valid address.");
      return false;
    } else {
      try {
        this.getDatabaseFile().transaction(async tx => {
          tx.executeSql('INSERT INTO Labels (Label, Address) VALUES (?,?)', [
            label,
            address,
          ]);
        });
        return true;
      } catch (error) {
        console.log(error);
        return false;
      }
    }
  }

  checkAddressValidity(userAddress) {
    global.Buffer = global.Buffer || require('buffer').Buffer;
    const pandacoin = require('bitcoinjs-lib');

    try {
      pandacoin.address.toOutputScript(userAddress);
      return true;
    } catch (error) {
      console.log('The inputted address has no matching script :(');
      return false;
    }
  }
}
