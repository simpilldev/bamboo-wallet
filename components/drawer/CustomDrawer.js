import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import colors from '../../assets/colors/colors';
import WalletSvg from '../../assets/imgs/wallet.svg';
import CoinsSvg from '../../assets/imgs/coins.svg';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {SafeAreaView} from 'react-native-safe-area-context';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LocalizedStrings from 'react-native-localization';

var localeFile = require('../drawer/locale.json');
let strings = new LocalizedStrings(localeFile);


const CustomDrawer = props => {
  return (
    <View style={{flex: 1, flexGrow: 1}}>
      <View style={styles.header}>
        <WalletSvg style={styles.headerSvgStyle} />
        <Text style={styles.walletTitleTextStyle}>Winston's Wallet</Text>
        <Text style={styles.walletAmountTextStyle}>1,000,000 PND</Text>
      </View>
      <DrawerContentScrollView
        {...props}
        contentContainerStyle={{
          paddingTop: 2,
          flex: 1,
        }}>
        <DrawerItemList {...props} />
      </DrawerContentScrollView>

      <View style={{padding: 20, borderTopWidth: 1, borderTopColor: '#ccc'}}>
        {/* <TouchableOpacity onPress={() => {}} style={{paddingVertical: 15}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text
              style={{
                fontSize: 15,
                fontFamily: 'Montserrat-Regular',
                marginLeft: 5,
              }}>
              {strings.whatIsPandacoin}
            </Text>
          </View>
        </TouchableOpacity> */}
        <TouchableOpacity onPress={() => {}} style={{paddingVertical: 15}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text
              style={{
                fontSize: 15,
                fontFamily: 'Montserrat-Regular',
                marginLeft: 5,
              }}>
              {strings.joinCommunity}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {}} style={{paddingVertical: 15}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text
              style={{
                fontSize: 15,
                fontFamily: 'Montserrat-Regular',
                marginLeft: 5,
              }}>
              {strings.help}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    paddingTop: 50, 
    backgroundColor: colors.backgroundGradientTop,
    paddingBottom: 16
  },

  headerSvgStyle: {
    height: 80,
    width: 80,
    marginLeft: 18,
  },

  walletTitleTextStyle: {
    color: '#fff',
    fontSize: 16,
    fontFamily: 'Montserrat-Medium',
    marginTop: 24,
    marginLeft: 18,
  },

  walletAmountTextStyle: {
    color: '#fff',
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0,
    marginTop: 12,
    marginBottom: 4,
    marginLeft: 18,
  },
});

export default CustomDrawer;
