import LinearGradient from 'react-native-linear-gradient';
import colors from '../../assets/colors/colors';
import * as React from 'react';

const Gradient = ({children}) => {
  return (
    <LinearGradient
      colors={[colors.backgroundGradientTop, colors.backgroundGradientBottom]}
      style={{flex: 1}}>
      {children}
    </LinearGradient>
  );
};

export default Gradient;
