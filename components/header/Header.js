import {
  StyleSheet,
  TouchableOpacity,
  _View,
} from 'react-native';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import QuestionSvg from '../../assets/imgs/question.svg';
import HamburgerSvg from '../../assets/imgs/hamburger.svg';
import * as React from 'react';

const Header = ({navigation}) => {
  const insets = useSafeAreaInsets();

  console.log('Insets = ' + insets.top)

  return (
    <SafeAreaView style={[styles.headerStyle, {marginTop: 60 - insets.top}]}>
      <TouchableOpacity onPress={() => navigation.openDrawer()}>
        <HamburgerSvg width={40} height={40} style={styles.headerItemStyle} />
      </TouchableOpacity>
      <TouchableOpacity>
        <QuestionSvg width={40} height={40} style={styles.headerItemStyle} />
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  headerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 32,
    alignItems: 'center',
  },

  headerItemStyle: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },
});

export default Header;
