import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import colors from "../../assets/colors/colors";
import Modal from 'react-native-modal'
import ModalStyles from "./ModalStyles";
import * as React from 'react'
import TextStyles from "../text/TextStyles";

export default function CustomModal({
  children, headerColor, headerText, isVisible, toggleModal, dualButtons,

  buttonColor, positiveButtonColor, negativeButtonColor,

  buttonText, positiveButtonText, negativeButtonText,

  onPress, onPositivePress, onNegativePress }) {

    console.log("Dual buttons is " + dualButtons)
  if (!dualButtons) {
    return (
      <View>
        <Modal
          isVisible={isVisible}
          animationIn={"zoomIn"}
          animationOut={"zoomOut"}
          useNativeDriver={true}
          useNativeDriverForBackdrop={true}
          onBackButtonPress={toggleModal}
          onBackdropPress={toggleModal}
          style={ModalStyles.modalContainer}>
          <Header headerText={headerText} headerColor={headerColor} />
          {children}
          <Buttons dualButtons={false} onPress={onPress} buttonText={buttonText} buttonColor={buttonColor} />
        </Modal>
      </View>
    );
  }

  else {
    return (
      <View>
        <Modal
          isVisible={isVisible}
          animationIn={"zoomIn"}
          animationOut={"zoomOut"}
          useNativeDriver={true}
          useNativeDriverForBackdrop={true}
          onBackButtonPress={toggleModal}
          onBackdropPress={toggleModal}
          style={ModalStyles.modalContainer}>
          <Header
            headerText={headerText}
            headerColor={headerColor} />
          {children}
          <Buttons
            dualButtons={true}
            onPositivePress={onPositivePress}
            onNegativePress={onNegativePress}
            positiveButtonText={positiveButtonText}
            negativeButtonText={negativeButtonText}
            positiveButtonColor={positiveButtonColor}
            negativeButtonColor={negativeButtonColor} />
        </Modal>
      </View>
    );
  }
}

const Buttons = ({ dualButtons, onPress, buttonText, buttonColor, onPositivePress, positiveButtonColor, positiveButtonText, onNegativePress, negativeButtonColor, negativeButtonText }) => {

  if (dualButtons) {
    var posColor, negColor;

    console.log('Negative Btn Color = ' + negativeButtonColor)

    switch (positiveButtonColor) {
      default:
      case 'green':
        posColor = ModalStyles.buttonStyle
        break
      case 'brown':
        posColor = ModalStyles.buttonStyleBrown
        break
      case 'red':
        posColor = ModalStyles.buttonStyleRed
        break
    }

    switch (negativeButtonColor) {
      case 'green':
        negColor = ModalStyles.buttonStyle
        break
      default:
      case 'brown':
        negColor = ModalStyles.buttonStyleBrown
        break
      case 'red':
        negColor = ModalStyles.buttonStyleRed
        break
    }

    return (
      <View style={ModalStyles.modalFooterDualButtons}>
        <TouchableOpacity style={negColor} onPress={onNegativePress} activeOpacity={0.5}>
          <Text style={ModalStyles.buttonTextStyle}>{negativeButtonText}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={posColor} onPress={onPositivePress} activeOpacity={0.5}>
          <Text style={ModalStyles.buttonTextStyle}>{positiveButtonText}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  else {
    var color;

    switch (buttonColor) {
      default:
      case 'green':
        color = ModalStyles.buttonStyle
        break
      case 'brown':
        color = ModalStyles.buttonStyleBrown
        break
      case 'red':
        color = ModalStyles.buttonStyleRed
        break
    }

    return (
      <View style={ModalStyles.modalFooter}>
        <TouchableOpacity style={color} onPress={onPress} activeOpacity={0.5}>
          <Text style={ModalStyles.buttonTextStyle}>{buttonText}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const Header = ({ headerColor, headerText }) => {
  var modalHeaderColor;

  console.log('Color = ' + headerColor)
  switch (headerColor) {
    case 'brown':
      modalHeaderColor = ModalStyles.modalHeaderBrown
      break
    case 'red':
      modalHeaderColor = ModalStyles.modalHeaderRed
      break
    case 'green':
    default:
      modalHeaderColor = ModalStyles.modalHeader
      break
  }

  return (
    <View>
      <View style={modalHeaderColor}>
        <Text style={TextStyles.modalTextTitleStyle}>{headerText}</Text>
      </View>
    </View>
  )
}