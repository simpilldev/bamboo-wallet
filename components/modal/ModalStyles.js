import { StyleSheet} from 'react-native'
import colors from '../../assets/colors/colors'

export default StyleSheet.create({
    modalContainer: {
        width: '80%',
        alignSelf: 'center'
    },

    modalHeader: {
        width: '100%',
        backgroundColor: colors.backgroundGradientTop,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'space-between',
    },

    modalTextTitleStyle: {
        color: 'white',
        textAlign: 'center',
        fontFamily: 'Montserrat-Regular',
        fontSize: 30,
        marginVertical: 12,
        marginLeft: 32
    },

    modalHeaderBrown: {
        width: '100%',
        backgroundColor: colors.modalBrown,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'space-between',
      },

      modalHeaderRed: {
        width: '100%',
        backgroundColor: colors.modalRed,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'space-between',
      },
    
      modalBody: {
        alignItems: 'center',
        backgroundColor: 'white',
        paddingVertical: 24
      },
    
      modalFooter: {
        alignItems: 'center',
        marginBottom: 20,
        flexDirection: "row",
        justifyContent: "center",
        paddingHorizontal: 32,
        backgroundColor: 'white',
        paddingBottom: 20,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20
      },

      modalFooterDualButtons: {
        alignItems: 'center',
        marginBottom: 20,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 32,
        backgroundColor: 'white',
        paddingBottom: 20,
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20
      },
    
      modalTextStyle: {
        color: '#141414',
        textAlign: 'left',
        fontFamily: 'Montserrat-Light',
        fontSize: 18,
        marginHorizontal: 32,
      },
    
      noButtonStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 20,
        elevation: 2,
        backgroundColor: colors.modalRed,
      },
    
      yesButtonStyle: {
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 20,
        elevation: 2,
        backgroundColor: colors.backgroundGradientTop,
      },

      buttonStyle: {
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 20,
        elevation: 2,
        backgroundColor: colors.backgroundGradientTop,
      },

      buttonStyleBrown: {
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 20,
        elevation: 2,
        backgroundColor: colors.modalBrown,
      },

      buttonStyleRed: {
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 20,
        elevation: 2,
        backgroundColor: colors.modalRed,
      },
      
    
      buttonTextStyle: {
        fontSize: 16,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: 'white',
      },
})