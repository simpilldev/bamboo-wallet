import {View, StyleSheet, Text} from 'react-native';
import CogSvg from '../../assets/imgs/cog.svg';
import HomeSvg from '../../assets/imgs/home.svg';
import PadlockSvg from '../../assets/imgs/padlock.svg';
import NetworkSvg from '../../assets/imgs/network.svg';
import CalculatorSvg from '../../assets/imgs/calculator.svg';
import InfoSvg from '../../assets/imgs/info.svg';
import colors from '../../assets/colors/colors';
import * as React from 'react';

import LocalizedStrings from 'react-native-localization';
import { TouchableOpacity } from 'react-native-gesture-handler';

var localeFile = require('../../screens/settings/locale.json');
let strings = new LocalizedStrings(localeFile);

const GENERAL = 1;
const SECURITY = 2;
const NETWORK = 3;
const TOOLS = 4;
const ABOUT = 5;

export default function SettingsBox({svgNumber}) {
  const Svg = ({svgNumber}) => {
    if (svgNumber == GENERAL) {
      return <CogSvg style={styles.svgStyle} />;
    } else if (svgNumber == SECURITY) {
      return <PadlockSvg style={styles.svgStyle} />;
    } else if (svgNumber == NETWORK) {
      return <NetworkSvg style={styles.svgStyle} />;
    } else if (svgNumber == TOOLS) {
      return <CalculatorSvg style={styles.svgStyle} />;
    } else if (svgNumber == ABOUT) {
      return <InfoSvg style={styles.svgStyle} />;
    } else {
      return console.error("Invalid Settings SVG number :(");
    }

    // switch (svgNumber) {
    //   case svgNumber == GENERAL:
    //     return <CogSvg style={styles.svgStyle} />;
    //   case svgNumber == SECURITY:
    //     return <PadlockSvg style={styles.svgStyle} />;
    //   case svgNumber == NETWORK:
    //     return <CogSvg style={styles.svgStyle} />;
    //   case svgNumber == TOOLS:
    //     return <CogSvg style={styles.svgStyle} />;
    //   case svgNumber == ABOUT:
    //     return <CogSvg style={styles.svgStyle} />;
    // }
  };

  var text;
   
    if (svgNumber == GENERAL) {
      text = strings.general;
    } else if (svgNumber == SECURITY) {
      text = strings.security;
    } else if (svgNumber == NETWORK) {
      text = strings.network;
    } else if (svgNumber == TOOLS) {
      text = strings.tools;
    } else if (svgNumber == ABOUT) {
      text =  strings.about;
    } else {
      console.error("Invalid Settings SVG number :(");
      text = "";
    }

  return (
    <TouchableOpacity style={styles.boxStyle}>
      <View style={styles.svgContainer}>
        <Svg svgNumber={svgNumber} />
      </View>
      <View style={styles.textsContainer}>
        <Text style={styles.settingTitleTextStyle}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  boxStyle: {
    marginHorizontal: 32,
    marginTop: 20,
    height: 80,
    backgroundColor: colors.wildWillow,
    borderColor: colors.gray27,
    borderRadius: 20,
    borderWidth: 1.5,
    flexDirection: 'row',
  },

  settingTitleTextStyle: {
    color: '#fff',
    fontSize: 24,
    fontFamily: 'Montserrat-Regular',
  },

  settingDescriptionTextStyle: {
    marginTop: 4,
    color: '#fff',
    fontSize: 16,
    fontFamily: 'Montserrat-Light',
  },

  svgContainer: {
    height: '100%',
    width: '27.5%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  svgStyle: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },

  textsContainer: {
    width: '65%',
    justifyContent: 'center',
  },
});
