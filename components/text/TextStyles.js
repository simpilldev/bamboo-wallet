import {StyleSheet} from 'react-native';
import colors from '../../assets/colors/colors';

export default StyleSheet.create({
  titleTextStyle: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 40,
    color: 'white',
  },

  regularTextStyle: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 20,
    color: 'white',
  },

  pndTickerStyle: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 32,
    color: colors.pndTickerColor,
  },

  pndBalanceStyle: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 48,
    color: 'white',
  },

  footerTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Medium',
    letterSpacing: 0.2,
    marginTop: 24,
    paddingRight: 2,
    fontSize: 18,
  },

  modalTextTitleStyle: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    fontSize: 30,
    marginVertical: 12,
    marginLeft: 32,
  },

  modalBodyTextStyle: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 16,
    color: 'black',
    backgroundColor: 'white',
    paddingHorizontal: 32,
    paddingVertical: 32,
  },

  modalBodyTextNoBottomPad: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 16,
    color: 'black',
    backgroundColor: 'white',
    paddingHorizontal: 32,
    paddingTop: 32,
  },


  drawerItemTextStyle: {
    marginLeft: -10,
    fontFamily: 'Montserrat-Regular',
    fontSize: 15,
  },
});
