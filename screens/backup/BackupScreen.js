import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  Button,
  TouchableOpacity,
  Image,
  _View,
  TextInput,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../assets/colors/colors';
import QuestionSvg from '../../assets/imgs/question.svg';
import HamburgerSvg from '../../assets/imgs/hamburger.svg';
import * as React from 'react';
import LocalizedStrings from 'react-native-localization';
import Header from '../../components/header/Header';
import Gradient from '../../components/gradient/Gradient';

var localeFile = require('./locale.json');
let strings = new LocalizedStrings(localeFile);

export default function BackupScreen({navigation}) {
  return (
    <Gradient>
      <Header navigation={navigation} />
    </Gradient>
  );
}

const styles = StyleSheet.create({});
