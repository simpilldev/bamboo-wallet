import {
  View,
  Text,
  Dimensions,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Button,
  Image,
  _View,
  TextInput,
  Pressable,
  Animated,
} from 'react-native';

import Swipeable from 'react-native-gesture-handler/Swipeable';

import {RecyclerListView, DataProvider, LayoutProvider} from 'recyclerlistview';
import Modal from 'react-native-modal';
import colors from '../../assets/colors/colors';
import LinearGradient from 'react-native-linear-gradient';
import QuestionSvg from '../../assets/imgs/question.svg';
import HamburgerSvg from '../../assets/imgs/hamburger.svg';
import BinSvg from '../../assets/imgs/bin.svg';
import SendSvg from '../../assets/imgs/send.svg';
import AddSvg from '../../assets/imgs/add.svg';
import PencilSvg from '../../assets/imgs/pencil.svg';
import * as React from 'react';
import LocalizedStrings from 'react-native-localization';
import CustomModal from '../../components/modal/CustomModal';
import SQLite from 'react-native-sqlite-storage';
import Gradient from '../../components/gradient/Gradient';
import {add} from 'react-native-reanimated';
import Header from '../../components/header/Header';
import {Buffer} from 'buffer';
import ModalStyles from '../../components/modal/ModalStyles';
import ContactsDatabaseHelper from '../../components/databases/ContactsDatabase';

global.Buffer = global.Buffer || require('buffer').Buffer;

var localeFile = require('./locale.json');
let strings = new LocalizedStrings(localeFile);

const ViewTypes = {
  FULL: 0,
};

const db = new ContactsDatabaseHelper();

export default function ContactsScreen({navigation, args}) {
  const [contextMenuModalVisible, setContextMenuModalVisible] =
    React.useState(false);
  const toggleContextMenuModal = () => {
    setContextMenuModalVisible(!contextMenuModalVisible);
  };

  const [errorModalVisible, setErrorModalVisible] = React.useState(false);
  const toggleErrorModal = () => {
    setErrorModalVisible(!errorModalVisible);
  };

  let row = [];
  let prevOpenedRow;

  const [label, setLabel] = React.useState('');
  const [address, setAddress] = React.useState('');

  React.useEffect(() => {
    db.createNewTable();
    db.getRowCount().then(value => {console.log('Output = ' + value)});
  }, []);

  let {width} = Dimensions.get('window');

  //Create the data provider and provide method which takes in two rows of data and return if those two are different or not.
  //THIS IS VERY IMPORTANT, FORGET PERFORMANCE IF THIS IS MESSED UP

  //Create the layout provider
  //First method: Given an index return the type of item e.g ListItemType1, ListItemType2 in case you have variety of items in your list/grid
  //Second: Given a type and object set the exact height and width for that type on given object, if you're using non deterministic rendering provide close estimates
  //If you need data based check you can access your data provider here
  //You'll need data in most cases, we don't provide it by default to enable things like data virtualization in the future
  //NOTE: For complex lists LayoutProvider will also be complex it would then make sense to move it to a different file
  const layoutProvider = new LayoutProvider(
    index => {
      return ViewTypes.FULL;
    },
    (type, dim) => {
      switch (type) {
        case ViewTypes.FULL:
          dim.width = width;
          dim.height = 120;
          break;
        default:
          dim.width = 0;
          dim.height = 0;
      }
    },
  );

  const rowRenderer = ({type, data, item, index}, onDelete, onClick) => {

    console.log('Index = ' + index)

    const closeRow = index => {
      console.log('closerow');
      if (prevOpenedRow && prevOpenedRow !== row[index]) {
        prevOpenedRow.close();
      }
      prevOpenedRow = row[index];
    };

    const renderLeftActions = (progress, dragX, onClick) => {
      const scale = dragX.interpolate({
        inputRange: [0, 100],
        outputRange: [0, 1],
      });
      return (
        <Animated.View
          style={{
            transform: [
              {
                scale,
              },
            ],
            marginLeft: 8,
            backgroundColor: '#bf6585',
            borderColor: '#fff',
            borderWidth: 1.5,
            borderRadius: 20,
            alignItems: 'center',
            justifyContent: 'center',
            width: 80,
            height: 100,
          }}>
          <BinSvg style={styles.svgStyle} />
        </Animated.View>
      );
    };

    const renderRightActions = (progress, dragX, onClick) => {
      const scale = dragX.interpolate({
        inputRange: [-100, 0],
        outputRange: [1, 0.1],
      });

      return (
        <Animated.View
          style={{
            transform: [
              {
                scale,
              },
            ],
            marginRight: 8,
            paddingLeft: 8,
            backgroundColor: '#6585bf',
            borderColor: colors.gray27,
            borderWidth: 1.5,
            borderRadius: 20,
            alignItems: 'center',
            justifyContent: 'center',
            width: 80,
            height: 100,
          }}>
          <PencilSvg style={styles.svgStyle} />
        </Animated.View>
      );
    };

    return (
      <View style={styles.swipeableContainer}>
        <Swipeable
          overshootLeft={false}
          overshootRight={false}
          useNativeAnimations={true}
          renderRightActions={(progress, dragX) =>
            renderRightActions(progress, dragX, onClick)
          }
          renderLeftActions={(progress, dragX) =>
            renderLeftActions(progress, dragX, onClick)
          }
          onSwipeableOpen={() => closeRow(index)}
          ref={ref => (row[index] = ref)}
          leftThreshold={50}
          rightThreshold={50}
          rightOpenValue={-100}
          leftOpenValue={-100}
          overshootFriction={1}
          friction={1}
          style={styles.swipeableContainer}>
          <View style={styles.container} onLongPress={toggleContextMenuModal}>
            <View style={styles.textsContainer}>
              <Text style={styles.labelTextStyle}>Test Name</Text>
              <Text style={styles.addressTextStyle} numberOfLines={1}>
                pn1qqq4tsllqcfepvs5e2f0v6ecydvtdu3nyekl2km
              </Text>
            </View>
            <View style={styles.btnContainer}>
              <TouchableOpacity>
                <SendSvg width={50} height={50} />
              </TouchableOpacity>
            </View>
          </View>
        </Swipeable>
      </View>
    );
  };

  const dataProvider = new DataProvider((r1, r2) => {
    return r1 !== r2;
  }).cloneWithRows(generateArray(4));

  return (
    <Gradient>
      <Header navigation={navigation} />

      <View
        style={{
          flex: 1,
          marginTop: 32,
        }}>
        <RecyclerListView
          layoutProvider={layoutProvider}
          dataProvider={dataProvider}
          rowRenderer={rowRenderer}
        />
      </View>

      <View style={styles.footerStyle}>
        <TouchableOpacity onPress={toggleContextMenuModal}>
          <AddSvg width={60} height={60} />
          <Text style={styles.footerTextStyle}>{strings.add}</Text>
        </TouchableOpacity>
      </View>

      <CustomModal
        isVisible={errorModalVisible}
        toggleModal={toggleErrorModal}
        dualButtons={false}
        headerColor={'brown'}
        headerText={'Uh Oh!'}
        buttonText={strings.ok}
        buttonColor={'brown'}
        onPress={() => {
          toggleErrorModal();
        }}>
        <View style={ModalStyles.modalBody}>
          <Text style={ModalStyles.modalTextStyle}>
            {strings.incorrectAddressEntered.replace('{0}', address)}
          </Text>
        </View>
      </CustomModal>

      <CustomModal
        isVisible={contextMenuModalVisible}
        toggleModal={toggleContextMenuModal}
        dualButtons={true}
        headerText={'Edit'}
        positiveButtonText={strings.done}
        negativeButtonText={strings.cancel}
        onPositivePress={() => {
          if(db.insertNewContact(label, address)) {
            console.log('True')
            toggleContextMenuModal();
          } else {
            console.log('False')
            toggleErrorModal();
          }
        }}
        onNegativePress={() => {
          toggleContextMenuModal();
        }}>
        <View style={styles.editModalBody}>
          <View style={styles.modalTextContainer}>
            <View
              style={{
                backgroundColor: 'white',
                height: '50%',
                flexDirection: 'column',
                justifyContent: 'center',
              }}>
              <Text style={styles.modalTextStyle}>{strings.name}</Text>
            </View>
            <View
              style={{
                backgroundColor: 'white',
                height: '50%',
                flexDirection: 'column',
                justifyContent: 'space-around',
              }}>
              <Text style={styles.modalTextStyle}>{strings.address}</Text>
            </View>
          </View>

          <View
            style={{
              backgroundColor: 'white',
              flexDirection: 'column',
              width: '60%',
            }}>
            <View
              style={{
                backgroundColor: 'white',
                flexDirection: 'column',
                height: '50%',
                justifyContent: 'center',
              }}>
              <TextInput
                style={styles.modalTextInputBox}
                placeholder={strings.labelBoxHint}
                placeholderTextColor="white"
                keyboardType="default"
                blurOnSubmit={false}
                underlineColorAndroid="#f000"
                returnKeyType="done"
                onChangeText={value => setLabel(value)}
              />
            </View>

            <View
              style={{
                backgroundColor: 'white',
                flexDirection: 'column',
                height: '50%',
                justifyContent: 'center',
              }}>
              <TextInput
                style={styles.modalTextInputBox}
                placeholder={strings.labelBoxHint}
                placeholderTextColor="white"
                keyboardType="default"
                blurOnSubmit={false}
                underlineColorAndroid="#f000"
                returnKeyType="done"
                onChangeText={value => setAddress(value)}
              />
            </View>
          </View>
        </View>
      </CustomModal>
    </Gradient>
  );
}

 function generateArray(number) {
  let arr = new Array(number);
  for (let i = 0; i < number; i++) {
    arr[i] = i;
  }
  return arr;
}

const styles = {
  swipeableContainer: {
    marginTop: 16,
    height: 100,
  },

  container: {
    height: 100,
    paddingVertical: 16,
    marginHorizontal: 32,
    justifyContent: 'space-around',
    borderRadius: 20,
    backgroundColor: colors.wildWillow,
    borderColor: colors.gray27,
    borderWidth: 1.5,
    alignItems: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
  },

  textsContainer: {
    flex: 0.75,
    flexDirection: 'column',
  },

  btnContainer: {
    flex: 0.25,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  footerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 20,
    alignItems: 'flex-end',
  },

  footerTextStyle: {
    color: 'white',
    marginRight: 8,
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.2,
    paddingTop: 16,
    paddingRight: 2,
    fontSize: 18,
  },

  labelTextStyle: {
    color: 'white',
    marginBottom: 8,
    marginLeft: 16,
    fontFamily: 'Montserrat-Regular',
    fontSize: 24,
  },

  addressTextStyle: {
    color: 'white',
    marginTop: 8,
    marginLeft: 16,
    fontFamily: 'Montserrat-Light',
    fontSize: 14,
  },

  modalTextInputBox: {
    width: '85%',
    justifyContent: 'space-around',
    borderRadius: 20,
    borderColor: '#141414',
    borderWidth: 1.5,
    alignItems: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
  },

  modalTextStyle: {
    marginLeft: 32,
    fontSize: 18,
    color: '#141414',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light',
  },

  editModalBody: {
    flexDirection: 'row',
    height: '30%',
    paddingVertical: 20,
    backgroundColor: 'white',
  },

  errorModalBody: {
    flexDirection: 'row',
    backgroundColor: 'white',
  },

  modalTextContainer: {
    backgroundColor: 'green',
    flexDirection: 'column',
    width: '40%',
  },

  svgStyle: {
    height: 60,
    width: 60,
    resizeMode: 'contain',
  },
};
