import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  Button,
  TouchableOpacity,
  Image,
  _View,
  TextInput,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../assets/colors/colors';
import QuestionSvg from '../../assets/imgs/question.svg';
import HamburgerSvg from '../../assets/imgs/hamburger.svg';
import SendWalletSvg from '../../assets/imgs/wallet_send.svg';
import ReceiveWalletSvg from '../../assets/imgs/wallet_receive.svg';
import SwapSvg from '../../assets/imgs/convert2.svg';
import * as React from 'react';
import LocalizedStrings from 'react-native-localization';
import Header from '../../components/header/Header';
import Gradient from '../../components/gradient/Gradient';

var localeFile = require('./locale.json');
let strings = new LocalizedStrings(localeFile);

export default function ConvertScreen({navigation}) {
  return (
    <Gradient>
      
      <Header navigation={navigation}/>

      <View style={styles.centerStyle}>
        <View style={styles.tickerContainer}>
          <Text style={styles.tickerStyle}>{strings.pnd}</Text>
          <Text style={styles.tickerStyle}>{strings.usd}</Text>
        </View>
        <View style={styles.textInputContainer}>
          <TextInput
            style={styles.textBoxStyle}
            placeholder={strings.addressBoxHint}
            placeholderTextColor="white"
            autoCapitalize="none"
            keyboardType="email-address"
            returnKeyType="next"
            underlineColorAndroid="#f000"
            blurOnSubmit={false}
          />
          <TextInput
            style={styles.textBoxStyle}
            placeholder={strings.addressBoxHint}
            placeholderTextColor="white"
            autoCapitalize="none"
            keyboardType="email-address"
            returnKeyType="next"
            underlineColorAndroid="#f000"
            blurOnSubmit={false}
          />
        </View>
      </View>

      <View style={styles.footerStyle}>
        <TouchableOpacity>
          <SwapSvg width={80} height={80} />
          <Text style={styles.footerTextStyle}>{strings.convert}</Text>
        </TouchableOpacity>
      </View>
    </Gradient>
  );
}

const styles = StyleSheet.create({
  centerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  tickerContainer: {
    flexDirection: 'column',
    paddingHorizontal: 32,
    justifyContent: 'space-between',
  },

  tickerStyle: {
    marginVertical: 24,
    fontFamily: 'Montserrat-Regular',
    fontSize: 32,
    color: 'white',
  },

  textInputContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },

  textBoxStyle: {
    color: 'white',
    marginVertical: 16,
    marginRight: 40,
    borderWidth: 1,
    borderRadius: 15,
    backgroundColor: colors.wildWillow,
    borderColor: colors.gray27,
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular',
    fontSize: 18,
  },

  footerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingBottom: 40,
    alignItems: 'flex-end',
  },

  footerWalletStyle: {
    alignItems: 'center',
  },

  footerItemStyle: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },

  footerTextStyle: {
    color: 'white',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.2,
    paddingTop: 16,
    paddingRight: 2,
    fontSize: 18,
  },
});
