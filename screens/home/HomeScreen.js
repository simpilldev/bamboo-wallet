import {
  StyleSheet,
  View,
  Text,
  Button,
  TouchableOpacity,
  Image,
  _View,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../assets/colors/colors';
import QuestionSvg from '../../assets/imgs/question.svg';
import HamburgerSvg from '../../assets/imgs/hamburger.svg';
import SendWalletSvg from '../../assets/imgs/wallet_send.svg';
import ReceiveWalletSvg from '../../assets/imgs/wallet_receive.svg';
import * as React from 'react';
import Animation from '../../components/animations/Animation';
import Header from '../../components/header/Header';
import Gradient from '../../components/gradient/Gradient';
import TextStyles from '../../components/text/TextStyles';

export default function HomeScreen({navigation}) {
  return (
    <Gradient>
      
      <Header navigation={navigation}/>

      <View style={styles.centerStyle}>
        <Text style={TextStyles.pndTickerStyle}>PND</Text>
        <Text style={TextStyles.pndBalanceStyle}>1,000.00</Text>
      </View>

      <View style={styles.footerStyle}>
        <View style={styles.footerWalletStyle}>
          <TouchableOpacity onPress={() => navigation.navigate('Send')}>
            <SendWalletSvg style={styles.footerItemStyle} />
          </TouchableOpacity>
          <Text style={TextStyles.footerTextStyle}>Send</Text>
        </View>
        <View style={styles.footerWalletStyle}>
          <TouchableOpacity onPress={() => navigation.navigate('Receive')}>
            <ReceiveWalletSvg style={styles.footerItemStyle} />
          </TouchableOpacity>
          <Text style={TextStyles.footerTextStyle}>Receive</Text>
        </View>
      </View>
    </Gradient>
  );
}

const styles = StyleSheet.create({
  centerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  footerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingBottom: 40,
    alignItems: 'flex-end',
  },

  footerWalletStyle: {
    alignItems: 'center',
  },

  footerItemStyle: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },
});
