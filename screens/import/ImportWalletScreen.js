import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import ModalStyles from '../../components/modal/ModalStyles';
import colors from '../../assets/colors/colors';
import QuestionSvg from '../../assets/imgs/question.svg';
import HamburgerSvg from '../../assets/imgs/hamburger.svg';
import * as React from 'react';
import CustomModal from '../../components/modal/CustomModal';
import Header from '../../components/header/Header';
import Gradient from '../../components/gradient/Gradient';
import LocalizedStrings from 'react-native-localization';

var localeFile = require('./locale.json');
let strings = new LocalizedStrings(localeFile);

export default function ImportWalletScreen({navigation}) {
  const [successModalVisible, setSuccessModalVisible] = React.useState(false);
  const toggleSuccessModal = () => {
    setSuccessModalVisible(!successModalVisible);
  };

  const [failureModalVisible, setFailureModalVisible] = React.useState(false);
  const toggleFailureModal = () => {
    setFailureModalVisible(!failureModalVisible);
  };

  return (
    <Gradient>
      
      <Header navigation={navigation}/>

      <View style={styles.importTitleContainer}>
        <Text style={styles.importTitleTextStyle}>{strings.walletImport}</Text>
      </View>

      <View style={styles.importTextBoxStyle}>
        <TextInput
          style={styles.importTextBoxTextStyle}
          placeholder={strings.walletImportInstructions}
          multiline={true}
          textAlignVertical="top"
          placeholderTextColor="aliceblue"
          keyboardType="default"
          blurOnSubmit={false}
          underlineColorAndroid="#f000"
          returnKeyType="done"
        />
      </View>

      <View style={styles.footerStyle}>
        <TouchableOpacity
          style={styles.createWalletButtonStyle}
          activeOpacity={0.5}
          onPress={toggleSuccessModal}>
          <Text style={styles.createWalletTextStyle}>
            {strings.importWallet}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.importWalletButtonStyle}>
          <Text style={styles.importWalletTextStyle}>{strings.scanOrFile}</Text>
        </TouchableOpacity>
      </View>

      <CustomModal
        headerColor={'green'}
        headerText={strings.successModalTitle}
        isVisible={successModalVisible}
        toggleModal={toggleSuccessModal}
        dualButtons={false}
        buttonText={strings.ok}
        onPress={() => {
          toggleSuccessModal();
        }}>
        <View style={ModalStyles.modalBody}>
          <Text style={ModalStyles.modalTextStyle}>
            {strings.successModalBody}
          </Text>
        </View>
      </CustomModal>

      <CustomModal
        headerColor={'brown'}
        headerText={strings.failureModalTitle}
        isVisible={failureModalVisible}
        toggleModal={toggleFailureModal}
        dualButtons={true}
        positiveButtonText={strings.ok}
        negativeButtonText={strings.help}
        onPositivePress={() => {
          toggleSuccessModal();
        }}
        onNegativePress={() => {
          toggleSuccessModal();
        }}>
        <View style={ModalStyles.modalBody}>
          <Text style={ModalStyles.modalTextStyle}>
            {strings.failureModalBody}
          </Text>
        </View>
      </CustomModal>
    </Gradient>
  );
}

const styles = StyleSheet.create({
  importTitleContainer: {
    paddingTop: 36,
    paddingHorizontal: 32,
  },

  importTitleTextStyle: {
    fontFamily: 'Montserrat-Regular',
    paddingLeft: 16,
    fontSize: 26,
    color: 'white',
  },

  importTextBoxStyle: {
    color: 'white',
    height: '40%',
    marginTop: 24,
    marginHorizontal: 42,
    borderWidth: 2,
    borderRadius: 15,
    borderColor: 'white',
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular',
    fontSize: 18,
  },

  importTextBoxTextStyle: {
    padding: 16,
    fontFamily: 'Montserrat-Light',
    fontSize: 18,
    color: 'white',
  },

  footerStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: 40,
    alignItems: 'center',
  },

  createWalletButtonStyle: {
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 18,
    borderRadius: 20,
    elevation: 2,
    backgroundColor: colors.wildWillow,
  },

  createWalletTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.1,
    fontSize: 18,
  },

  importWalletButtonStyle: {
    marginTop: 16,
  },

  importWalletTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.1,
    fontSize: 14,
  },
});
