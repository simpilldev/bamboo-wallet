import {
  StyleSheet,
  View,
  Text,
  Button,
  TouchableOpacity,
  Image,
  _View,
  TextInput,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../assets/colors/colors';
import QuestionSvg from '../../assets/imgs/question.svg';
import HamburgerSvg from '../../assets/imgs/hamburger.svg';
import DiceSvg from '../../assets/imgs/dice.svg';
import InfoSvg from '../../assets/imgs/info.svg';
import {SafeAreaView} from 'react-native-safe-area-context';
import * as React from 'react';
import LocalizedStrings from 'react-native-localization';
import {RecyclerListView} from 'recyclerlistview';
import Header from '../../components/header/Header';
import Gradient from '../../components/gradient/Gradient';
import TextStyles from '../../components/text/TextStyles';
import CustomModal from '../../components/modal/CustomModal';
import ModalStyles from '../../components/modal/ModalStyles';
import LottieView from 'lottie-react-native';
import BambooTextInput from '../../components/BambooTextInput';

var localeFile = require('./locale.json');
let strings = new LocalizedStrings(localeFile);

export default function InactiveScreen({navigation}) {
  const [passwordModalVisible, setPasswordModalVisible] = React.useState(false),
    [noInternetModalVisible, setNoInternetModalVisible] = React.useState(false),
    togglePasswordModal = () => {
      setPasswordModalVisible(!passwordModalVisible);
    },
    toggleNoInternetModal = () => {
      setNoInternetModalVisible(!noInternetModalVisible);
    };

  return (
    <Gradient
      colors={[colors.backgroundGradientTop, colors.backgroundGradientBottom]}
      style={styles.linearGradientStyle}>
      <View style={styles.centerStyle}>
        <Text style={[styles.safetyFirstStyle, TextStyles.titleTextStyle]}>
          Welcome back
        </Text>

        <Text style={[styles.createPasswordStyle, TextStyles.regularTextStyle]}>
          I noticed you were away so I closed your wallet for you :)
        </Text>
        <LottieView
          style={styles.animationStyle}
          source={require('../../assets/imgs/37101-cute-little-panda-sleeping.json')}
          autoPlay={true}
          loop={true}
          resizeMode="cover"
        />
      </View>

      <View style={styles.footerStyle}>
        <TouchableOpacity
          style={styles.createWalletButtonStyle}
          activeOpacity={0.5}
          onPress={() => {
            toggleNoInternetModal();
          }}>
          <Text style={styles.createWalletTextStyle}>Open Wallet</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.importWalletButtonStyle}>
          <Text style={styles.importWalletTextStyle}>
            Change Inactivity Timer
          </Text>
        </TouchableOpacity>
      </View>

      <CustomModal
        toggleModal={togglePasswordModal}
        isVisible={passwordModalVisible}
        dualButtons={true}
        headerText={'Password'}
        positiveButtonText={'Done'}
        negativeButtonText={'Cancel'}
        onPositivePress={() => {
          togglePasswordModal();
        }}
        onNegativePress={() => {
          togglePasswordModal();
        }}>
        <Text
          style={[TextStyles.regularTextStyle, TextStyles.modalBodyTextStyle]}>
          Please enter your password below to unlock your wallet.
        </Text>
        <View
          style={{
            backgroundColor: 'white',
            justifyContent: 'center',
            paddingLeft: 32,
            paddingBottom: 32,
          }}>
            <BambooTextInput
            backgroundColor={'white'}
            fontSize={14}
            onSubmit={console.log('Submitted')}
            />
        </View>
      </CustomModal>

      <CustomModal
        toggleModal={toggleNoInternetModal}
        isVisible={noInternetModalVisible}
        dualButtons={true}
        headerText={'No Internet'}
        headerColor={'red'}
        positiveButtonText={'Ok'}
        negativeButtonText={'More Info'}
        negativeButtonColor={'red'}
        onPositivePress={() => {
          toggleNoInternetModal();
        }}
        onNegativePress={() => {
          toggleNoInternetModal();
        }}>
        <Text
          style={[
            TextStyles.regularTextStyle,
            TextStyles.modalBodyTextNoBottomPad,
          ]}>
          Bamboo wallet was unable to connect to the internet :(
        </Text>
        <View
          style={{
            backgroundColor: 'white',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <LottieView
            style={{
              width: 180,
              height: 180,
              resizeMode: 'contain',
              backgroundColor: 'white',
            }}
            source={require('../../assets/imgs/11645-no-internet-animation.json')}
            autoPlay={true}
            loop={true}
            resizeMode="cover"
          />
        </View>
      </CustomModal>
    </Gradient>
  );
}

const styles = StyleSheet.create({
  animationStyle: {
    width: 250,
    height: 250,
    alignSelf: 'center',
    justifyContent: 'center'
  },
  centerStyle: {
    flex: 1,
    flexGrow: 1,
    justifyContent: 'center',
    marginHorizontal: 32,
    paddingTop: 60,
  },

  safetyFirstStyle: {
    marginTop: 80,
  },

  createPasswordStyle: {
    marginTop: 32,
  },

  passwordContainerStyle: {
    width: '100%',
    height: 80,
    borderRadius: 20,
    backgroundColor: colors.wildWillow,
    borderColor: '#fff',
    borderWidth: 1.5,
  },

  textInputStyle: {
    flex: 1,
    paddingStart: 24,
    textAlignVertical: 'center',
  },

  footerStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 40,
    alignItems: 'center',
  },

  createWalletButtonStyle: {
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 18,
    borderRadius: 20,
    elevation: 2,
    backgroundColor: colors.wildWillow,
  },

  createWalletTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.1,
    fontSize: 18,
  },

  importWalletButtonStyle: {
    marginTop: 16,
  },

  importWalletTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.1,
    fontSize: 14,
  },
});
