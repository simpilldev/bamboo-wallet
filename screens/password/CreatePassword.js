import {
  StyleSheet,
  View,
  Text,
  Button,
  TouchableOpacity,
  Image,
  _View,
  TextInput,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../assets/colors/colors';
import QuestionSvg from '../../assets/imgs/question.svg';
import HamburgerSvg from '../../assets/imgs/hamburger.svg';
import DiceSvg from '../../assets/imgs/dice.svg';
import InfoSvg from '../../assets/imgs/info.svg';
import {SafeAreaView} from 'react-native-safe-area-context';
import * as React from 'react';
import LocalizedStrings from 'react-native-localization';
import {RecyclerListView} from 'recyclerlistview';
import Header from '../../components/header/Header';
import Gradient from '../../components/gradient/Gradient';
import TextStyles from '../../components/text/TextStyles';
import CustomModal from '../../components/modal/CustomModal';
import ModalStyles from '../../components/modal/ModalStyles';

var localeFile = require('./locale.json');
let strings = new LocalizedStrings(localeFile);

export default function CreatePasswordScreen({navigation}) {
  const [badPasswordModalVisible, setBadPasswordModalVisible] =
    React.useState(false);
  const toggleBadPasswordModal = () => {
    setBadPasswordModalVisible(!badPasswordModalVisible);
  };

  return (
    <Gradient
      colors={[colors.backgroundGradientTop, colors.backgroundGradientBottom]}
      style={styles.linearGradientStyle}>
      <View style={styles.centerStyle}>
        <Text style={[styles.safetyFirstStyle, TextStyles.titleTextStyle]}>
          Safety First.
        </Text>

        <Text style={[styles.createPasswordStyle, TextStyles.regularTextStyle]}>
          Please choose a password for your Pandacoin wallet.
        </Text>
        <View style={styles.passwordContainerStyle}>
          <TextInput
            style={[TextStyles.regularTextStyle, styles.textInputStyle]}
            placeholder="Enter your password"
            placeholderTextColor="white"
            autoCapitalize="none"
            keyboardType="email-address"
            returnKeyType="next"
            underlineColorAndroid="#f000"
            blurOnSubmit={false}
          />
        </View>
        <View style={[styles.passwordContainerStyle, {marginTop: 16}]}>
          <TextInput
            style={[TextStyles.regularTextStyle, styles.textInputStyle]}
            placeholder="Re-enter your password"
            placeholderTextColor="white"
            autoCapitalize="none"
            keyboardType="email-address"
            returnKeyType="next"
            underlineColorAndroid="#f000"
            blurOnSubmit={false}
          />
        </View>
      </View>

      <View style={styles.footerStyle}>
        <View style={styles.footerWalletStyle}>
          <TouchableOpacity
            onPress={() => {
              toggleBadPasswordModal(), console.log('Pressssssssss');
            }}>
            <DiceSvg style={styles.footerItemStyle} />
          </TouchableOpacity>
          <Text style={styles.footerTextStyle}>Generate</Text>
        </View>
        <View style={styles.footerWalletStyle}>
          <TouchableOpacity onPress={() => {}}>
            <InfoSvg style={styles.footerItemStyle} />
          </TouchableOpacity>
          <Text style={styles.footerTextStyle}>Advice</Text>
        </View>
      </View>

      <CustomModal
        toggleModal={toggleBadPasswordModal}
        isVisible={badPasswordModalVisible}
        dualButtons={true}
        positiveButtonText={'Yes'}
        negativeButtonText={'No'}
        headerColor= {'brown'}
        headerText= {'Bad Password'}
        onPositivePress={() => {
          toggleBadPasswordModal();
        }}
        onNegativePress={() => {
          toggleBadPasswordModal();
        }}>
            <Text style={[TextStyles.regularTextStyle, TextStyles.modalBodyTextStyle]}>{strings.badPasswordModalBody}</Text>
      </CustomModal>
    </Gradient>
  );
}

const styles = StyleSheet.create({
  centerStyle: {
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: 32,
    paddingBottom: 120,
  },

  safetyFirstStyle: {
    marginTop: 80,
  },

  createPasswordStyle: {
    marginTop: 32,
    marginBottom: 80,
  },

  passwordContainerStyle: {
    width: '100%',
    height: 80,
    borderRadius: 20,
    backgroundColor: colors.wildWillow,
    borderColor: '#fff',
    borderWidth: 1.5,
  },

  textInputStyle: {
    flex: 1,
    paddingStart: 24,
    textAlignVertical: 'center',
  },

  footerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingBottom: 40,
    alignItems: 'flex-end',
  },

  footerWalletStyle: {
    alignItems: 'center',
  },

  footerItemStyle: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },

  footerTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.2,
    paddingTop: 16,
    paddingRight: 2,
    fontSize: 18,
  },
});
