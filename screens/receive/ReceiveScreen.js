import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import Modal from 'react-native-modal';
import CustomModal from '../../components/modal/CustomModal';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../assets/colors/colors';
import QuestionSvg from '../../assets/imgs/question.svg';
import HamburgerSvg from '../../assets/imgs/hamburger.svg';
import SendWalletSvg from '../../assets/imgs/wallet_send.svg';
import ReceiveWalletSvg from '../../assets/imgs/wallet_receive.svg';
import QrCodeSvg from '../../assets/imgs/qr_code.svg';
import BagSvg from '../../assets/imgs/bag.svg';
import ContactSvg from '../../assets/imgs/contacts.svg';
import CopySvg from '../../assets/imgs/copy.svg';
import * as React from 'react';
import LocalizedStrings from 'react-native-localization';
import Header from '../../components/header/Header';
import Gradient from '../../components/gradient/Gradient';

var localeFile = require('./locale.json');
let strings = new LocalizedStrings(localeFile);

export default function ReceiveScreen({navigation}) {
  const [qrCodeModalVisible, setQrCodeModalVisible] = React.useState(false);
  const toggleQrCodeModal = () => {
    setQrCodeModalVisible(!qrCodeModalVisible);
  };

  const [addressCopiedModalVisible, setAddressCopiedModalVisible] =
    React.useState(false);
  const toggleAddressCopiedModal = () => {
    setAddressCopiedModalVisible(!addressCopiedModalVisible);
  };

  return (
    <Gradient>
        <KeyboardAwareScrollView
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: 'space-between',
            flexDirection: 'column',
          }}>
          <Header navigation={navigation}/>

          <View style={styles.textBoxesStyle}>
            <View style={styles.entryStyle}>
              <Text style={styles.smallTextStyle}>
                {strings.useAddressToReceivePnd}
              </Text>
            </View>
          </View>

          <View style={styles.footerStyle}>
            <View style={styles.footerWalletStyle}>
              <TouchableOpacity onPress={toggleAddressCopiedModal}>
                <CopySvg width={80} height={80} />
              </TouchableOpacity>
              <Text style={styles.footerTextStyle}>{strings.copyButton}</Text>
            </View>

            <View style={styles.footerWalletStyle}>
              <TouchableOpacity onPress={toggleQrCodeModal}>
                <QrCodeSvg width={80} height={80} />
              </TouchableOpacity>
              <Text style={styles.footerTextStyle}>{strings.showQrButton}</Text>
            </View>
          </View>

          <CustomModal
            isVisible={addressCopiedModalVisible}
            toggleModal={toggleAddressCopiedModal}
            headerText={strings.addressCopiedModalTitle}
            dualButtons={false}
            buttonText={strings.gotIt}
            onPress={() => {
              toggleAddressCopiedModal();
            }}>
            <View style={styles.modalBody}>
              <Text style={styles.modalTextStyle}>
                {strings.addressCopiedModalBody}
              </Text>
            </View>
          </CustomModal>

          <CustomModal
            isVisible={qrCodeModalVisible}
            toggleModal={toggleQrCodeModal}
            headerText={strings.showQrModalTitle}
            dualButtons={false}
            buttonText={strings.gotIt}
            onPress={() => {
              toggleQrCodeModal();
            }}>
            <View style={styles.modalBody}>
              <Text style={styles.modalTextStyle}>
                {strings.showQrModalBody}
              </Text>
            </View>
          </CustomModal>
        </KeyboardAwareScrollView>
    </Gradient>
  );
}

const styles = StyleSheet.create({
  entryStyle: {
    flexDirection: 'row',
    marginTop: 16,
    marginHorizontal: 42,
    marginVertical: 20,
  },
  
  textBoxesStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    marginTop: 20,
    alignItems: 'center',
  },

  textBoxStyle: {
    flex: 1,
    color: 'white',
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 2,
    borderRadius: 15,
    borderColor: 'white',
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular',
    fontSize: 18,
  },

  smallTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    fontSize: 20,
  },

  svgStyle: {
    marginLeft: 20,
    marginVertical: 20,
    resizeMode: 'contain',
  },

  centerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  pndTickerStyle: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 32,
    color: colors.pndTickerColor,
  },

  pndBalanceStyle: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 48,
    color: 'white',
  },

  footerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingBottom: 40,
  },

  footerWalletStyle: {
    alignItems: 'center',
  },

  footerItemStyle: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },

  footerTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.2,
    paddingTop: 16,
    paddingRight: 2,
    fontSize: 18,
  },

  modalContainer: {
    width: '80%',
    alignSelf: 'center',
  },

  modalHeader: {
    width: '100%',
    backgroundColor: colors.backgroundGradientTop,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'space-between',
  },

  modalHeaderBrown: {
    width: '100%',
    backgroundColor: '#AA4A44',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'space-between',
  },

  modalTextTitleStyle: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular',
    fontSize: 30,
    marginVertical: 12,
    marginLeft: 32,
  },

  modalBody: {
    alignItems: 'center',
    backgroundColor: 'white',
    paddingVertical: 24,
  },

  modalFooter: {
    alignItems: 'center',
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal: 32,
    backgroundColor: 'white',
    paddingBottom: 20,
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
  },

  modalTextStyle: {
    color: '#141414',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light',
    fontSize: 18,
    marginHorizontal: 32,
  },

  noButtonStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 20,
    elevation: 2,
    backgroundColor: '#AA4A44',
  },

  yesButtonStyle: {
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 20,
    elevation: 2,
    backgroundColor: colors.backgroundGradientTop,
  },

  buttonTextStyle: {
    fontSize: 16,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },

  modalImgStyle: {
    backgroundColor: 'white',
    width: '100%',
    marginVertical: 0,
    resizeMode: 'contain',
  },
});
