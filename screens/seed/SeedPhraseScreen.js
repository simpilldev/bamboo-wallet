import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  Button,
  TouchableOpacity,
  Image,
  _View,
  Animated,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../assets/colors/colors';
import DiceSvg from '../../assets/imgs/dice.svg';
import InfoSvg from '../../assets/imgs/warning.svg';
import * as React from 'react';
import LocalizedStrings from 'react-native-localization';
import Header from '../../components/header/Header';
import Gradient from '../../components/gradient/Gradient';
import LottieView from 'lottie-react-native';
import TextStyles from '../../components/text/TextStyles';
import CustomModal from '../../components/modal/CustomModal';

var localeFile = require('./locale.json');
let strings = new LocalizedStrings(localeFile);

let animation = new Animated.Value(0);

export default function SeedPhraseScreen({navigation}) {
  const [seedPhraseModalVisible, setSeedPhraseModalVisible] =
    React.useState(false);
  const toggleSeedPhraseModal = () => {
    setSeedPhraseModalVisible(!seedPhraseModalVisible);
  };

  return (
    <Gradient>
      <Header navigation={navigation} />

      <View style={styles.seedPhraseTitleContainer}>
        <Text style={styles.seedPhraseTitleTextStyle}>Your Seed Phrase:</Text>
      </View>

      <View style={styles.seedPhraseBoxesContainer}>
        <View style={styles.seedPhraseBoxStyle}>
          <Text style={styles.seedPhraseTextStyle}>1. donor</Text>
          <Text style={styles.seedPhraseTextStyle}>3. stereo</Text>
          <Text style={styles.seedPhraseTextStyle}>5. kind</Text>
          <Text style={styles.seedPhraseTextStyle}>7. wage</Text>
          <Text style={styles.seedPhraseTextStyle}>9. best</Text>
          <Text style={styles.seedPhraseTextStyle}>11. fortune</Text>
        </View>

        <View style={styles.seedPhraseBoxStyle}>
          <Text style={styles.seedPhraseTextStyle}>2. twenty</Text>
          <Text style={styles.seedPhraseTextStyle}>4. faith</Text>
          <Text style={styles.seedPhraseTextStyle}>6. dune</Text>
          <Text style={styles.seedPhraseTextStyle}>8. system</Text>
          <Text style={styles.seedPhraseTextStyle}>10. surprise</Text>
          <Text style={styles.seedPhraseTextStyle}>12. core</Text>
        </View>
      </View>

      <TouchableOpacity style={styles.footerStyle} onPress={() => toggleSeedPhraseModal()}>
        <LottieView
          style={styles.footerItemStyle}
          source={require('../../assets/imgs/lf30_editor_wnyrcb26.json')}
          autoPlay={true}
          loop={true}
          resizeMode="cover"
        />
        <Text style={TextStyles.footerTextStyle}>Warning</Text>
      </TouchableOpacity>

      <CustomModal
        toggleModal={toggleSeedPhraseModal}
        isVisible={seedPhraseModalVisible}
        headerColor={'red'}
        headerText={'Caution'}
        dualButtons={true}
        negativeButtonColor={'red'}
        negativeButtonText={strings.learnMore}
        positiveButtonText={strings.ok}
        onNegativePress={() => toggleSeedPhraseModal()}
        onPositivePress={() => toggleSeedPhraseModal()}>
          <Text style={[TextStyles.regularTextStyle, TextStyles.modalBodyTextStyle]}>{strings.seedPhraseModalBody}</Text>
        </CustomModal>
    </Gradient>
  );
}

const styles = StyleSheet.create({
  seedPhraseTitleContainer: {
    margin: 32,
  },

  seedPhraseTitleTextStyle: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 26,
    color: 'white',
  },

  seedPhraseBoxesContainer: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 32,
    justifyContent: 'space-around',
  },

  seedPhraseBoxStyle: {
    width: '45%',
  },

  seedPhraseTextStyle: {
    backgroundColor: colors.wildWillow,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: colors.gray27,
    fontFamily: 'Montserrat-Regular',
    paddingHorizontal: 15,
    marginVertical: 6,
    paddingVertical: 15,
    fontSize: 16,
    textAlignVertical: 'center',
    color: 'white',
  },

  footerStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    paddingBottom: 40,
    alignItems: 'center',
  },

  footerWalletStyle: {
    alignItems: 'center',
  },

  footerItemStyle: {
    width: 120,
    height: 120,
    marginBottom: -20,
    resizeMode: 'contain',
  },

  footerTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.2,
    paddingTop: 16,
    paddingRight: 2,
    fontSize: 18,
  },

  shakeStyle: {
    transform: [
      {
        translateX: animation,
      },
    ],
  },
});
