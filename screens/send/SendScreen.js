import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  Button,
  TouchableOpacity,
  Image,
  _View,
  TextInput,
  Animated,
  Pressable,
} from 'react-native';

import Modal from 'react-native-modal';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../assets/colors/colors';
import QuestionSvg from '../../assets/imgs/question.svg';
import HamburgerSvg from '../../assets/imgs/hamburger.svg';
import SendWalletSvg from '../../assets/imgs/wallet_send.svg';
import ReceiveWalletSvg from '../../assets/imgs/wallet_receive.svg';
import QrCodeSvg from '../../assets/imgs/qr_code.svg';
import BagSvg from '../../assets/imgs/bag.svg';
import ContactSvg from '../../assets/imgs/contacts-book.svg';
import * as React from 'react';
import LocalizedStrings from 'react-native-localization';
import CustomModal from '../../components/modal/CustomModal';
import Gradient from '../../components/gradient/Gradient';
import Header from '../../components/header/Header';

var localeFile = require('./locale.json');
let strings = new LocalizedStrings(localeFile);

export default function SendScreen({navigation}) {
  const [sendModalVisible, setSendModalVisible] = React.useState(false);
  const toggleSendModal = () => {
    setSendModalVisible(!sendModalVisible);
  };

  const [missingEntryModalVisible, setMissingEntryModalVisible] =
    React.useState(false);
  const toggleMissingEntryModal = () => {
    setMissingEntryModalVisible(!missingEntryModalVisible);
  };

  return (
    <Gradient>
        <KeyboardAwareScrollView
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: 'space-between',
            flexDirection: 'column',
          }}>
          <Header navigation={navigation}/>

          <View style={styles.textBoxesContainer}>
            <View style={styles.textBoxContainer}>
              <View style={styles.textContainer}>
                <TextInput
                  style={styles.textStyle}
                  placeholder={strings.addressBoxHint}
                  placeholderTextColor="white"
                  autoCapitalize="none"
                  keyboardType="email-address"
                  returnKeyType="next"
                  underlineColorAndroid="#f000"
                  blurOnSubmit={false}
                />
              </View>
              <View style={styles.btnContainer}>
                <TouchableOpacity>
                  <QrCodeSvg width={40} height={40} />
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.textBoxContainer}>
              <View style={styles.textContainer}>
                <TextInput
                  style={styles.textStyle}
                  placeholder={strings.amountBoxHint}
                  placeholderTextColor="white"
                  autoCapitalize="none"
                  keyboardType="email-address"
                  returnKeyType="next"
                  underlineColorAndroid="#f000"
                  blurOnSubmit={false}
                />
              </View>
              <View style={styles.btnContainer}>
                <TouchableOpacity>
                  <BagSvg width={40} height={40} />
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.textBoxContainer}>
              <View style={styles.textContainer}>
                <TextInput
                  style={styles.textStyle}
                  placeholder={strings.labelBoxHint}
                  placeholderTextColor="white"
                  autoCapitalize="none"
                  keyboardType="email-address"
                  returnKeyType="next"
                  underlineColorAndroid="#f000"
                  blurOnSubmit={false}
                />
              </View>
              <View style={styles.btnContainer}>
                <TouchableOpacity>
                  <ContactSvg width={36} height={36} />
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View style={styles.footerStyle}>
            <View style={styles.footerWalletStyle}>
              <TouchableOpacity onPress={toggleSendModal}>
                <SendWalletSvg style={styles.footerItemStyle} />
              </TouchableOpacity>
              <Text style={styles.footerTextStyle}>{strings.send}</Text>
            </View>
          </View>

          <CustomModal
            headerText={strings.failureModalTitle}
            isVisible={sendModalVisible}
            toggleModal={toggleSendModal}
            dualButtons={true}
            positiveButtonText={strings.yes}
            negativeButtonText={strings.no}
            onPositivePress={() => {
              toggleSendModal();
            }}
            onNegativePress={() => {
              toggleSendModal();
            }}>
            <View style={styles.modalBody}>
              <Text style={styles.modalTextStyle}>
                {strings.sendConfirmationModalBody}
              </Text>
            </View>
          </CustomModal>

          <CustomModal
            headerText={strings.missingEntryModalTitle}
            headerColor={'brown'}
            isVisible={missingEntryModalVisible}
            toggleModal={toggleMissingEntryModal}
            dualButtons={false}
            buttonText={strings.ok}
            buttonColor={'brown'}
            onPress={() => {
              toggleMissingEntryModal();
            }}>
            <View style={styles.modalBody}>
              <Text style={styles.modalTextStyle}>
                {strings.missingEntryModalBody}
              </Text>
            </View>
          </CustomModal>

        </KeyboardAwareScrollView>
    </Gradient>
  );
}

const styles = StyleSheet.create({
  textBoxContainer: {
    marginHorizontal: 32,
    marginVertical: 16,
    justifyContent: 'space-around',
    borderRadius: 20,
    backgroundColor: colors.wildWillow,
    borderColor: colors.gray27,
    borderWidth: 1.5,
    alignItems: 'flex-start',
    height: 100,
    flexDirection: 'row',
    alignItems: 'center',
  },

  textContainer: {
    flex: 0.75,
    flexDirection: 'column',
  },

  textStyle: {
    color: 'white',
    marginTop: 8,
    marginLeft: 32,
    fontFamily: 'Montserrat-Regular',
    fontSize: 18,
  },

  btnContainer: {
    flex: 0.25,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonTextStyle: {
    fontSize: 16,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },

  entryStyle: {
    flexDirection: 'row',
    height: 80,
    marginTop: 16,
    marginLeft: 42,
    marginRight: 32,
    marginVertical: 20,
  },

  textBoxesContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    marginTop: 20,
    alignItems: 'center',
  },

  textBoxStyle: {
    flex: 1,
    color: 'white',
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 2,
    borderRadius: 15,
    borderColor: 'white',
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular',
    fontSize: 18,
  },

  svgStyle: {
    marginLeft: 20,
    marginVertical: 20,
    resizeMode: 'contain',
  },

  centerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  footerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingBottom: 40,
    alignItems: 'flex-end',
  },

  footerWalletStyle: {
    alignItems: 'center',
  },

  footerItemStyle: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },

  footerTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.2,
    paddingTop: 16,
    paddingRight: 2,
    fontSize: 18,
  },

  modalBackGround: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainer: {
    width: '80%',
    alignSelf: 'center',
  },

  modalHeader: {
    width: '100%',
    backgroundColor: colors.backgroundGradientTop,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'space-between',
  },

  modalHeaderBrown: {
    width: '100%',
    backgroundColor: '#AA4A44',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'space-between',
  },

  modalTextTitleStyle: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular',
    fontSize: 30,
    marginVertical: 12,
    marginLeft: 32,
  },

  modalBody: {
    alignItems: 'center',
    backgroundColor: 'white',
    paddingVertical: 24,
  },

  modalFooter: {
    alignItems: 'center',
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 32,
    backgroundColor: 'white',
    paddingBottom: 20,
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
  },

  modalTextStyle: {
    color: '#141414',
    textAlign: 'left',
    fontFamily: 'Montserrat-Light',
    fontSize: 18,
    marginHorizontal: 32,
  },

  noButtonStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 20,
    elevation: 2,
    backgroundColor: '#AA4A44',
  },

  yesButtonStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 20,
    elevation: 2,
    backgroundColor: colors.backgroundGradientTop,
  },
});
