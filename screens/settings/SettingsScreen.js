import {
  StyleSheet,
  SafeAreaView,
  View,
  TouchableOpacity,
  Box,
  Text,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../assets/colors/colors';
import QuestionSvg from '../../assets/imgs/question.svg';
import HamburgerSvg from '../../assets/imgs/hamburger.svg';
import * as React from 'react';
import LocalizedStrings from 'react-native-localization';
import Header from '../../components/header/Header';
import Gradient from '../../components/gradient/Gradient';
import SettingsSvg from '../../assets/imgs/cog.svg';
import SettingsBox from '../../components/settings/SettingsBox';

var localeFile = require('./locale.json');
let strings = new LocalizedStrings(localeFile);

const GENERAL = 1;
const SECURITY = 2;
const NETWORK = 3;
const TOOLS = 4;
const ABOUT = 5;

export default function SettingsScreen({navigation}) {
  return (
    <Gradient>
      <Header navigation={navigation} />

      <SettingsBox svgNumber={GENERAL}/>
      <SettingsBox svgNumber={SECURITY}/>
      <SettingsBox svgNumber={ABOUT}/>

      {/* <View style={styles.boxStyle}>
        <View style={styles.svgContainer}>
          <SettingsSvg style={styles.svgStyle} />
        </View>
        <View style={styles.textsContainer}>
          <Text style={styles.settingTitleTextStyle}>General</Text>
          <Text style={styles.settingDescriptionTextStyle}>
            General settings for Bamboo
          </Text>
        </View>
      </View> */}
    </Gradient>
  );
}

const styles = StyleSheet.create({});
