import { StyleSheet, SafeAreaView, View, Text, Button, TouchableOpacity, Image, _View, Pressable, Touchable } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import colors from '../../assets/colors/colors'
import WalletSvg from '../../assets/imgs/wallet.svg'
import * as React from 'react';
import LocalizedStrings from 'react-native-localization'
import Gradient from '../../components/gradient/Gradient';

var localeFile = require('./locale.json')
let strings = new LocalizedStrings(localeFile)

export default function WelcomeScreen({ navigation }) {
  return (
    <Gradient>

      <View style={styles.centerStyle}>
        <WalletSvg style={styles.centerImageStyle} />
        <Text style={styles.welcomeTextStyle}>{strings.welcome}</Text>
        <Text style={styles.manageTextStyle}>{strings.briefDescription}</Text>
      </View>

      <View style={styles.footerStyle}>

        <TouchableOpacity style={styles.createWalletButtonStyle} activeOpacity={0.5}>
          <Text style={styles.createWalletTextStyle}>{strings.createWallet}</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.importWalletButtonStyle}>
          <Text style={styles.importWalletTextStyle}>{strings.importWallet}</Text>
        </TouchableOpacity>
      </View>

    </Gradient>
  );
}
const styles = StyleSheet.create({
  createWalletButtonStyle: {
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 18,
    borderRadius: 20,
    elevation: 2,
    backgroundColor: colors.wildWillow,
  },

  createWalletTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.1,
    fontSize: 18
  },

  importWalletButtonStyle: {
    marginTop: 16
  },

  importWalletTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.1,
    fontSize: 14
  },

  welcomeTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.1,
    paddingTop: 50,
    fontSize: 26
  },

  manageTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Light',
    letterSpacing: 0.1,
    marginHorizontal: 32,
    paddingTop: 25,
    fontSize: 18
  },

  centerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  },

  centerImageStyle: {
    width: 200,
    height: 200,
    resizeMode: "contain",
  },

  footerStyle: {
    flexDirection: 'column',
    justifyContent: 'center',
    paddingBottom: 40,
    alignItems: 'center'
  }
});