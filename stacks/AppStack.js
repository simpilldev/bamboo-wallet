import 'react-native-gesture-handler';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  Button,
  TouchableOpacity,
  Image,
  _View,
} from 'react-native';
import * as React from 'react';
import HomeScreen from '../screens/home/HomeScreen';
import SeedPhraseScreen from '../screens/seed/SeedPhraseScreen';
import ConvertScreen from '../screens/convert/ConvertScreen';
import ContactsScreen from '../screens/contacts/ContactsScreen';
import BackupScreen from '../screens/backup/BackupScreen';
import SettingsScreen from '../screens/settings/SettingsScreen';
import WelcomeScreen from '../screens/welcome/WelcomeScreen';
import SendScreen from '../screens/send/SendScreen';
import ReceiveScreen from '../screens/receive/ReceiveScreen';
import TransactionsScreen from '../screens/transactions/TransactionsScreen';
import {createDrawerNavigator} from '@react-navigation/drawer';
import CustomDrawer from '../components/drawer/CustomDrawer';
import colors from '../assets/colors/colors';

import HomeSvg from '../assets/imgs/home.svg';
import MagnifierSvg from '../assets/imgs/magnifier.svg';
import ContactsSvg from '../assets/imgs/contacts.svg';
import ConvertSvg from '../assets/imgs/convert.svg';
import KeySvg from '../assets/imgs/key.svg';
import CogSvg from '../assets/imgs/cog.svg';
import WalletSvg from '../assets/imgs/wallet.svg';
import {Buffer} from 'buffer';
import {StackActions} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

global.Buffer = global.Buffer || require('buffer').Buffer;

const Drawer = createDrawerNavigator();

const Stack = createNativeStackNavigator();

export default AppStack = () => {
  return (
    <Drawer.Navigator
      drawerContent={props => <CustomDrawer {...props} />}
      screenOptions={{
        headerShown: false,
        drawerActiveBackgroundColor: colors.backgroundGradientBottom,
        drawerActiveTintColor: '#fff',
        drawerInactiveTintColor: '#333',
        drawerLabelStyle: {
          marginLeft: -10,
          fontFamily: 'Montserrat-Medium',
          fontSize: 16,
        },
      }}>
      <Drawer.Screen
        name="Home"
        component={HomeScreen}
        options={{
          drawerIcon: () => <HomeSvg style={styles.drawerImgStyle} />,
        }}
      />
      <Drawer.Screen
        name="Transactions"
        component={TransactionsScreen}
        options={{
          drawerIcon: () => <MagnifierSvg style={styles.drawerImgStyle} />,
        }}
      />
      <Drawer.Screen
        name="Convert"
        component={ConvertScreen}
        options={{
          drawerIcon: () => <ConvertSvg style={styles.drawerImgStyle} />,
        }}
      />
      <Drawer.Screen
        name="Contacts"
        component={ContactsScreen}
        options={{
          drawerIcon: () => <ContactsSvg style={styles.drawerImgStyle} />,
        }}
      />
      <Drawer.Screen
        name="Seed Phrase"
        component={SeedPhraseScreen}
        options={{
          drawerIcon: () => <KeySvg style={styles.drawerImgStyle} />,
        }}
      />
      <Drawer.Screen
        name="Settings"
        component={SettingsScreen}
        options={{
          drawerIcon: () => <CogSvg style={styles.drawerImgStyle} />,
        }}
      />
      <Stack.Screen
        name="Send"
        component={SendScreen}
        options={{headerShown: false, drawerItemStyle: {height: 0}}}
      />
      <Stack.Screen
        name="Receive"
        component={ReceiveScreen}
        options={{headerShown: false, drawerItemStyle: styles.hideFromDrawerStyle}}
      />
    </Drawer.Navigator>
  );
};

const styles = StyleSheet.create({
  drawerImgStyle: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },

  hideFromDrawerStyle: {
    height: 0,
    width: 0
  },

  drawerSvgStyle: {
    width: 40,
    height: 40,
  },
});
