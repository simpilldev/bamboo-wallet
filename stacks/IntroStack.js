import "react-native-gesture-handler";
import { StyleSheet, SafeAreaView, View, Text, Button, TouchableOpacity, Image, _View } from "react-native";
import * as React from 'react';
import WelcomeScreen from '../screens/welcome/WelcomeScreen'
import ImportWalletScreen from '../screens/import/ImportWalletScreen'
import CreatePasswordScreen from "../screens/password/CreatePassword";
import InactiveScreen from "../screens/inactive/InactiveScreen";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const Stack = createNativeStackNavigator();

export default IntroStack = () => {
  return (
        <Stack.Navigator initialRouteName='Inactive' screenOptions={{ headerShown: false }}>
        <Stack.Screen name='Welcome' component={WelcomeScreen} />
        <Stack.Screen name='Import' component={ImportWalletScreen} />
        <Stack.Screen name='Password' component={CreatePasswordScreen} />
        <Stack.Screen name='Inactive' component={InactiveScreen} />
      </Stack.Navigator>

  );
}